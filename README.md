# TráficoChilango API
API para TráficoChilango App. 

TráficoChilango App tiene la finalidad de proveer a los usuarios del Sistema de Transporte Colectivo Metro (STCM) de la Cd. de México una herramienta para crear y ver reportes de incidentes en dicho sistema ayudandoles así a establecer rutas alternas en su viaje.

### Version
0.1.1 (Dev)

### Tech

* [Node js]
* [Express]
* [Passport]
* [Social oAuth]
* [Mongoose]

### Configure

Create config.js file in root directory:

```
#!javascript

// config.js
module.exports = {
	port: 4000,
	db: {
		database: [MONGO Connection]
	},
	// == Session config ==
	sessionOpts : {
		secret: [SecretToken],
		store : null,
		saveUninitialized: true,
		resave: false,
		secure: true
	},
	//PUBLIC
	facebook:{
	 	appID : [Facebook appID],
	 	appSecret : [Facebook appSecret],
	 	callbackUrl : [Callback URL]
	 },
	// PUBLIC
	twitter:{
		apikey : [Twitter apikey],
		apisecret : [Twitter apiSecret],
		callbackURL : [Callback URL]
	},
	// PUBLIC
	google:{
		clientID : [Google ClientID],
		clientSecret : [Google clientSecret],
		callbackURL : [Callback URL]3
	}
}
```