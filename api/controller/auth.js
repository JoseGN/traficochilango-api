// == Modules dependencies ==
var Config = require('../../config.js'),
    moment = require('moment'),
    User = require('../models/users'),
    jwt = require('jwt-simple');


module.exports = {
    /*
    |--------------------------------------------------------------------------
    | Middleware para sesión express
    |--------------------------------------------------------------------------
    */
    ensureAuthenticated: function(req, res, next) {
        
        if (!req.headers.authorization) {
          return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
        }   
        
        var token = req.headers.authorization.split(' ')[1];

        var payload = null;
        try {
          payload = jwt.decode(token, Config.sessionOpts.secret);
        }

        catch (err) {
            return res.status(401).send({ message: 'Unauthorized' });
        }

        if (payload.exp <= moment().unix()) {
            return res.status(401).send({ message: 'This token has expired' });
        }

        User.findById(payload.sub, function (err, user){
            if (err) {
                return res.status(500).send({
                    success: false,
                    message: 'Internal server error. /Find user Auth.'
                })
            }

            if (!user) {
                return res.status(400).send({
                    success: false,
                    message: 'User missing'
                })
            }

            req.user = user;
            next();
        })

    },

    /*
    |--------------------------------------------------------------------------
    | Middleware para generar tokens
    |--------------------------------------------------------------------------
    */
    createJWT: function(user) {
        var payload = {
            sub: user._id,
            iat: moment().unix(),
            exp: moment().add(14, 'days').unix()
        };
        return jwt.encode(payload, Config.sessionOpts.secret);
    }
}