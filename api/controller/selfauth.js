//  Login with OAuth developed by Jesús Hernández

//== Modules dependecies==

var logger = require('morgan'),
	request = require('request'),
	Config = require('../../config.js'),
	User = require('../models/users'),
	Mailer = require('../utils/mailer'),
	jwt = require('jwt-simple'),
	qs = require('querystring'),
 	Auth = require('./auth');

function loginSelf (app, router) {

	/*
	|--------------------------------------------------------------------------
	| Login with Facebook
	|--------------------------------------------------------------------------
	*/
	router.route('/facebook')
		.post(function (req, res){

			var accessTokenUrl = 'https://graph.facebook.com/v2.3/oauth/access_token',
  				graphApiUrl = 'https://graph.facebook.com/v2.3/me';
  				
  			var params = {
				code: req.body.code,
			    client_id: req.body.clientId,
			    client_secret: '101137277013c2facaab8c9f49c72ff3',
			    redirect_uri: req.body.redirectUri
			};

			// Si esta presente un token refrescar el token
			if (req.headers.authorization) {
				 var token = req.headers.authorization.split(' ')[1];

		        var payload = null;
		        try {
		          payload = jwt.decode(token, Config.sessionOpts.secret);
		        }

		        catch (err) {
		            return res.status(500).send({ message: err.message });
		        }

		        User.findById(payload.sub, function (err, user){
		            if (err) {
		                return res.status(500).send({
		                    success: false,
		                    message: 'Internal server error. /Find user Auth.'
		                })
		            }

		            if (!user) {
		                return res.status(400).send({
		                    success: false,
		                    message: 'User missing'
		                })
		            }

		            var token = Auth.createJWT(user);
		            return res.status(200).send({
		            	token:token
		            })		            	
		            
		        })

			}else{

				// Cambiar authcode por un token
				request.get({ url: accessTokenUrl, qs: params, json: true }, function(err, response, accessToken) {
					if (response.statusCode !== 200) {
						return res.status(500).send({ message: accessToken.error.message });
					}

				    request.get({ url: graphApiUrl, qs: accessToken, json: true }, function(err, response, profile) {
				    	if (response.statusCode !== 200) {
				    		return res.status(500).send({ message: profile.error.message });
				    	}
				    	
				        // Crear una nueva cuenta de usuario o devolver una existente.
				        User.findOne({ facebook: profile.id }, function(err, existingUser) {

				    		// Recuperar información sobre el perfil del usuario si ya está registrado.
				        	if (existingUser) {
				        		var token = Auth.createJWT(existingUser);
				        		return res.send({ token: token });
				        	}

				        	var user = new User();
				        	user.id_social = profile.id;
				        	user.avatar = 'https://graph.facebook.com/' + profile.id + '/picture?type=large';
				        	user.nombre = profile.name;
				        	user.since = new Date();
				        	user.redes_sociales.push(profile); 
				        	user.save(function() {
				        		var token = Auth.createJWT(user);
				        		res.send({ token: token });
				        	});
				        });
					   
					});
				});
			}	
				
		})

	/*
	|--------------------------------------------------------------------------
	| Login with Twitter
	|--------------------------------------------------------------------------
	*/
	router.route('/twitter')
		.post(function(req, res) {

			var requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
			var accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
			var profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';

		  	// iniciar petición
		  	if (!req.body.oauth_token || !req.body.oauth_verifier) {
			  	var requestTokenOauth = {
			  		consumer_key: Config.twitter.apikey,
			  		consumer_secret: Config.twitter.apisecret,
			  		callback: req.body.redirectUri
			  	};

			    // Obtener autorización de TT
			    request.post({ url: requestTokenUrl, oauth: requestTokenOauth }, function(err, response, body) {
			    	var oauthToken = qs.parse(body);

			      // Enviar pantalla
			      res.send(oauthToken);
			  	});
			} else {
			    // Segunda petición 
			    var accessTokenOauth = {
			    	consumer_key: Config.twitter.apikey,
			    	consumer_secret: Config.twitter.apisecret,
			    	token: req.body.oauth_token,
			    	verifier: req.body.oauth_verifier
			    };

		    // Cambiar auth code por un token
		    request.post({ url: accessTokenUrl, oauth: accessTokenOauth }, function(err, response, accessToken) {

		    	accessToken = qs.parse(accessToken);

		    	var profileOauth = {
		    		consumer_key: Config.twitter.apikey,
		    		consumer_secret: Config.twitter.apisecret,
		    		oauth_token: accessToken.oauth_token
		    	};

			    // Obtener información del usuario
			    request.get({
			    	url: profileUrl + accessToken.screen_name,
			    	oauth: profileOauth,
			    	json: true
			    }, function(err, response, profile) {

		        
		        // Verificar para crear nueva cuanta
		        User.findOne({ twitter: profile.id }, function(err, existingUser) {
		         	if (existingUser) {
		         		return res.send({ token: createJWT(existingUser) });
		         	}

		         	var user = new User();

		         	user.id_social = profile.id;
		        	user.avatar = profile.profile_image_url.replace('_normal', '');
		        	user.nombre = profile.name;
		        	user.since = new Date();
		        	user.redes_sociales.push(profile); 
		         	user.save(function() {
		         		res.send({ token: createJWT(user) });
		         	});
		        })
		      
		  	});
		});
	}	
});

	/*
	|--------------------------------------------------------------------------
	| Create Email and Password Account
	|--------------------------------------------------------------------------
	*/
	router.route('/email/signup')
		.post(function(req, res) {

		  	User.findOne({ email: req.body.email })
		  	.exec(function (err, user) {
		  	  	console.log(user)
			  	if(user){
					return res.status(400).send({
						successs: false,
						message: 'User '+ req.body.email +' already exist.'
					})
				}	

		  	 	var user = new User({
		  	 	  	nombre: req.body.nombre,
		  	 	  	email: req.body.email,
		  	 	  	provider: 'Email',
		  	 	  	password: new User().generateHash(req.body.password)
		  	 	});

				//return res.send({ token: createJWT(user) });

		  	 	user.save(function() {
		  	 	  	res.send({ token: Auth.createJWT(user) });
		  	 	});

		  	});
		});


	/*
	|--------------------------------------------------------------------------
	| Log in with Email
	|--------------------------------------------------------------------------
	*/
		router.route('/email/login')
		.post(function(req, res) {
			User.findOne({ email: req.body.email })
			.exec(function(err, user) {


				if (!user) {

					return res.status(401).send({ 
						message: 'Wrong email and/or password'
					});

				}				

				if (!user.validPassword(req.body.password)) {
					return res.status(401).send({	
						message: 'Wrong email and/or password' 
					});
				}

				res.send({ 
					token: Auth.createJWT(user)
				});
				
			});
		});

}

module.exports = loginSelf