/*user.js*/
/*controllers*/

/*
* AmetroMX
* @autor Jesús Hernández
* @colaborator José García
*
*/

var User = require('../models/users'),
	isLogged = require('../utils/logged'),
	Mailer = require('../utils/mailer'),
	tools = require('../utils/tools'),
	Auth = require('./auth');

var userController = function (app, router) {

	router.route('/user')
		//======================================================
		// Actulizar usuario
		//======================================================
		.put(Auth.ensureAuthenticated, function (req, res){

			var user = req.user,
				email = user.email,
				body = req.body;

			User.findOneAndUpdate({
				email:email
			},{
				nombre:body.nombre,
				avatar:body.avatar,
				estado:body.estado,
				telefono:body.telefono,
				redes_sociales:body.redes_sociales
			})
			.exec(function (err, userAct){

				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error/ update user.'
					})
				}

				if (userAct == null || !userAct) {
					return res.status(400).send({
						success: false,
						message: 'User '+ email +'doesn´t exist.'
					})
				};

				user.nombre = body.nombre;
				user.avatar = body.avatar;
				user.estado = body.estado;
				user.telefono = body.telefono;
				user.redes_sociales = body.redes_sociales;
				user.password = null;
				user.follows = userAct.folllows;
				user.followers = userAct.folllowers;

				res.status(200).send({
					token: Auth.createJWT(user)
				})

			})

		})

		//======================================================
		// Muestra todos los usuarios registrados
		//======================================================
		.get(Auth.ensureAuthenticated, function (req, res) {

			User.find({}, function (err, users) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}
				res.status(201).send({
					success:true,
					usuarios: users
				})
			})
		})

	router.route('/actualizar/password')
		//======================================================
		// Actualizar contraseña
		//======================================================
		.post(Auth.ensureAuthenticated, function (req, res){
			var user = req.user,
				body = req.body,
				new_pass = body.new_pass,
				ok_pass = body.ok_pass;

			if (new_pass != ok_pass) {
				return res.status(400).send({
					success: false,
					message: 'Confirmacion de password incorrecta'
				})
			};

			encrypt_pass = user.generateHash(new_pass);

			User.findOneAndUpdate({
				email:user.email
			},{
				password:encrypt_pass
			})
			.exec(function (err, userFind){

				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error /actualizar/password.'
					})
				}

				res.status(200).send({
					success:true,
					usuario: 'Password actualizado'
				})
			});
		})


	router.route('/recuperar/password')
		//======================================================
		// Recuperar contraseña
		//======================================================
		.post(function (req, res){

			var body = req.body,
				email = body.email,
				password = tools.password.generate();

			User.findOne({
				email:email
			})
			.exec(function (err, user){
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error / recuperar.'
					})
				}

				var new_passEncrypted = user.generateHash(password);

				User.findOneAndUpdate(
					{
						email: email
					},{
						password: new_passEncrypted
					}
				).exec(function (err, userAct){
					if (err) {
						console.log(err)
						return res.status(500).send({
							success: false,
							message: 'Internal server error / recuperar.'
						})
					}

					if (userAct == null || !userAct || userAct == false) {
						return res.status(400).send({
							success: true,
							message: "User doesn´t exist"
						})
					};

					var data_mail = {
				    	name: userAct.nombre,
				    	email: email,
				    	password: password
				    };

			    	var mail = new Mailer(email, 'Recuperar password AmetroMX ✔', 'recuperar', data_mail);
					mail.sendMail(function (err){

						if (err) {
							console.log(err)
							return res.status(500).send({
								success: false,
								message: 'Internal server error / send error Mailer.'
							})
						}

						res.status(200).send({
							success: true,
							message: userAct
						})
					});

				});
			})

		})


	router.route('/user/follow/:id')
		//======================================================
		// Guarda id and email de un usuario al cual desea seguir
		//======================================================
		.post(Auth.ensureAuthenticated, function (req, res){

			var id = req.params.id,
				user = req.user,
				user_email = req.user.email,
				user_id = req.user._id;

			User.findOne({
				_id:id
			})
			.exec(function (err, follow){
				if (err) {
					console.log(err);
					return res.status(500).send({
						success: false,
						message: "Internal Server Error. db-user-follow"
					});
				}

				if (follow == null || !follow) {
					return res.status(400).send({
						success: false,
						message: "User " + id +" not found."
					});
				}
				// Verifico que no se siga ya anteriormete al usuario
				for (var i = 0; i < follow.followers.length; i++) {
					if (follow.followers[i].email == user_email) {
						return res.status(400).send({
							success: false,
							message: "You already follow to: " + follow.email
						});
					};
				};
				// Guardo seguidor y seguido
				follow.followers.push({"id":user_id, "email": user_email});
				user.follows.push({"id":follow._id, "email": follow.email});

				followSave(follow, user, function (err, result){
					if (err) {
						console.log(err);
						return res.status(500).send({
							success: false,
							message: "Internal Server Error. db-user-follow Update-funcion"
						});
					}
					user.password = null;
					res.status(200).send({
						success: false,
						usuario: result
					});
				});
			});
		});

	router.route('/user/unfollow/:id')
		//======================================================
		// Guarda id and email de un usuario al cual desea seguir
		//======================================================
		.post(Auth.ensureAuthenticated, function (req, res){

			var id = req.params.id,
				user = req.user,
				user_email = req.user.email,
				user_id = req.user._id,
				flag = false;

			User.findOne({
				_id:id
			})
			.exec(function (err, follow){
				if (err) {
					console.log(err);
					return res.status(500).send({
						success: false,
						message: "Internal Server Error. db-user-follow"
					});
				}

				if (follow == null || !follow) {
					return res.status(400).send({
						success: false,
						message: "User " + id +" not found."
					});
				}
				// Verifico que se siga al usuario y elimino al usuario de sus followers
				for (var i = 0; i < follow.followers.length; i++) {
					if (follow.followers[i].email == user_email) {
						follow.followers.splice(i, 1);
						flag = true;
					};
				};
				// Sin no se seguia al usuario respondo un error
				if (!flag) {
					return res.status(400).send({
						success: false,
						message: "You doesn't follow to: " + id
					});
				};
				// Elimino al follow de follows de usuario
				for (var i = 0; i < user.follows.length; i++) {
					if (user.follows[i].id == id) {
						user.follows.splice(i, 1);
					};
				};

				followSave(follow, user, function (err, result){
					if (err) {
						console.log(err);
						return res.status(500).send({
							success: false,
							message: "Internal Server Error. db-user-follow Update-funcion"
						});
					}
					user.password = null;
					res.status(200).send({
						success: false,
						usuario: result
					});
				});
			});
		});

	//======================================================
	// Muestra información del usuario y su sesión activa
	//======================================================
	router.route('/user/profile')
		.get(Auth.ensureAuthenticated, function (req, res) {
			req.user.password = null;
			res.status(200).send({
				success:true,
				usuario: req.user
			})
		})

	//======================================================
	// Elimina la sesión
	//======================================================
	router.route('/logout')
		.get(function (req, res) {
			req.logout()
			res.status(200).send({
				success:true,
				usuario: 'Logout do'
			})
		})
}

function followSave(follow, follower, result){
	User.findOneAndUpdate(
		{
			email:follow.email
		},{
			followers:follow.followers
		}
	)
	.exec(function (err){
		if (err) {
			console.log(err);
			return result({
				success: false,
				message: "Internal Server Error. db-user-follow"
			});
		}
		User.findOneAndUpdate(
			{
				email:follower.email
			},{
				follows:follower.follows
			}
		).exec(function (err){
			if (err) {
				console.log(err);
				return result({
					success: false,
					message: "Internal Server Error. db-user-follower"
				});
			}
			result(null, follower)
		})
	})
}

module.exports = userController