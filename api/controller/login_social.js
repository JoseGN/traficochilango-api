var passport = require('passport'),
	auth  = require('./auth'),
	User = require('../models/users'),
	isLogged = require('../utils/logged'),
	Mailer = require('../utils/mailer'),
	tools = require('../utils/tools'),
	request = require('request');

var loginSocial = function (app, router) {
	
	router.route('/facebook/token')
		//======================================================
		// Login con Facebook Token
		// Esta ruta está orientada a la versión móvil
		//======================================================
		.post(function (req, res, next){
			passport.authenticate('facebook-token', function (err, user) {
				
				req.login(user, function (err, result){
					if (err) {
						return res.status(500).send({
							success: true,
							message: 'Internal server facebook-token Login'
						})
					}

					user.password = null;
					user.redes_sociales = null;

					res.status(200).send({
						success: true,
						usuario: user
					});
				})
			})(req, res, next);			
		});
	
	router.route('/facebook')
		//======================================================
		// Registro con FaceBook
		//======================================================
		.get(passport.authenticate('facebook', 
			{ scope : ['email', 'public_profile', 'publish_actions']}
		));

	router.route('/facebook/callback')
		//======================================================
		// Login con FaceBook CallBack
		//======================================================
		.get(passport.authenticate('facebook', {
				successRedirect : '/app#/reports',
				failureRedirect : '/'
			})
		);

	router.route('/twitter')
		//======================================================
		// Registro con Twitter
		//======================================================
		.get(passport.authenticate('twitter'));

	router.route('/twitter/callback')
		//======================================================
		// Login con Twitter CallBack
		//======================================================
		.get(passport.authenticate('twitter', {
				successRedirect : '/app#/reports',
				failureRedirect : '/'
			})
		);

	router.route('/twitter/token')
		//======================================================
		// Login con Twitter Token
		// Esta ruta está orientada a la versión móvil
		//======================================================
		.post(passport.authenticate('twitter-token'),
		  function (req, res) {
		    // do something with req.user
		    if(req.user){
		    	user = req.user
		    	user.password = null
		    	user.redes_sociales = null

		    	res.status(200).send({
		    		success: true,
		    		message: user
		    	})
		    }
		  }
		);

	router.route('/google')
		//======================================================
		// Registro con Google
		//======================================================
		.get(passport.authenticate('google', { scope: ['profile', 'email']}));

	router.route('/google/callback')
		//======================================================
		// Login con Google CallBack
		//======================================================
		.get(passport.authenticate('google', {
				successRedirect : '/app#/reports',
				failureRedirect : '/'
			})
		);

}

module.exports = loginSocial

