/*reports.js*/
/*controllers*/

/*
* AmetroMX
* @autor Jesús Hernández
* @colaborator José García
*
*/
var passport = require('passport'),
	Auth  = require('./auth'),
	Report = require('../models/reports'),
	modelreportGlobal = require('../models/reportGlobal'),
	modelUser = require('../models/users'),
	modelLineas = require('../utils/lineas-metro'),
	isLogged = require('../utils/logged'),
	moment = require('moment'),
	date = new Date(),
	gcm = require('./gcm'),
	jwt = require('jsonwebtoken');
	var gcm = require('node-gcm');

	moment.locale("es");

var reportController = function (app, router) {

	router.route('/reporte')
		//======================================================
		// Crea un reporte de un usuario
		//======================================================
		.post(Auth.ensureAuthenticated, function (req, res) {
			var body = req.body,
				usr = req.user,
				email = usr.email;
			// Verificar si se enviaron archivos
			if (req.files.imagen) {
				var imagen = req.files.imagen,
					imagenBuffer = imagen.buffer,
					imagenB64 = new Buffer(imagenBuffer).toString('base64'),
					flag = false;
			}else{
				imagenB64 = null;
			}

			// Obtener info de estaciones
			for (var i = 0; i < modelLineas.length; i++) {
				if (modelLineas[i].linea == body.linea) {
					for (var j = 0; j < modelLineas[i].estaciones.length; j++) {
						if (modelLineas[i].estaciones[j].estacion == body.estacion) {
							logo = modelLineas[i].estaciones[j].imagen;
							flag = true;
						}
					}
				}
			}

			if (!flag) {
				console.log("Error on find station :" + body.estacion)
				return res.status(500).send({
					success: false,
					message: 'Internal server error: Create report/"Error on find station :' + body.estacion
				});
			};

			var	data = {
				linea: body.linea,
				estacion: body.estacion,
				logo:logo,
				comentario: body.comentario,
				imagen:imagenB64,
				user: email,
				user_name: usr.nombre,
				user_id: usr.id,
				user_avatar: usr.avatar,
				date: new Date()
			}

			Report.findOne({
				linea:data.linea,
				estacion: data.estacion,
				user_id: usr.id
			})
			.sort({date: -1})
			.exec(function (err, report) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error/ Create report'
					})
				}

				if (report) {
					// Verificar antiguedad del último reporte en la misma ubicación
					var ago = moment(new Date(report.date)),
						now = moment(new Date()),
						diff = now.diff(ago, 'minutes');
					if (diff < 5) {
						var less = 60-diff
						return res.status(400).send({
							success: false,
							message: 'Tu último reporte de la línea '+ body.linea + ' en la estación ' + body.estacion + ', fue hace ' + diff + ' minutos. Espera por favor ' + less + ' minutos más para reportar en esta ubicación nuevamente.'
						})
					}
				}

				var report = new Report(data);
				report.save(function (err, report) {
					if (err) {
						console.log(err)
						return res.status(500).send({
							success: false,
							message: 'Internal server error/ Create report'
						})
					}

					// Evaluar reporte
					analitic(data);

					res.status(200).send({
						success: true,
						reporte: report
					})
				})

			})

		})

		//======================================================
		// Muestra todos los reportes de los usuarios
		//======================================================
		.get(function (req, res) {
			Report.find({})
			.select('-control -compare')
			.exec(function (err, reports) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}
				res.status(201).send({
					success:true,
					reportes: reports
				})
			})
		})

	router.route('/gcm').get(function (req, res){
		/*gcm('e', function (err, res){
			res.status(200).send(res)
		})*/

		// Set up the sender with you API key
		var message = new gcm.Message();
		var sender = new gcm.Sender('AIzaSyDEJsMIayctH5s5KzAZEVVYDzHAPQg11Fg');

		// Dispositivos
		var regIds = [780551948946];

		// Notificación
		message.addNotification({
		  title: 'Alert!!!',
		  body: 'Abnormal Jorge'
		});
		// Data
		//message.addData('key1', 'msg1');

		message.collapseKey = 'demo';
		message.delayWhileIdle = true;
		message.timeToLive = 3000;
		//message.dryRun = true;

		//Now the sender can be used to send messages
		sender.send(message, regIds, 4, function (err, result) {
		    if(err){
		    	console.error('err',err);

		    }

		    console.log('1', result);

		});

		sender.sendNoRetry(message, regIds, function (err, result) {
		    if(err){
		    	console.error(err);

		    }

		    console.log(2, result);

		});
		res.status(200).send('ok');
	})

	router.route('/consuelas/').post(function (req, res){

		return res.status(200).send({
			req: req.user
		})

	})

	router.route('/reporte/user/:init/:limit')
		//======================================================
		// Muestra reportes de un usuario sin detalles
		//======================================================
		.get(Auth.ensureAuthenticated, function (req, res) {
			var email= req.user.email,
				user = req.user,
				init = req.params.init,
				limit = req.params.limit;

			Report.find({
				user_id:user.id
			})
			.select('id date estacion linea logo')
			.sort({date: -1})
			.skip(init+1)
			.limit(limit)
			.exec(function (err, reports) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}

				res.status(201).send({
					success:true,
					reportes: reports
				})
			})
		})

	router.route('/reporte/id/:id')
		//======================================================
		// Muestra información de un reporte a detalle
		//======================================================
		.get(function (req, res) {
			var id= req.params.id;
			Report.findOne({
				_id:id
			})
			.select('-control -compare')
			.exec(function (err, report) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}
				res.status(201).send({
					success:true,
					reporte: report
				})
			})
		});

	router.route('/reporte/general/lite/:init/:limit')
		//======================================================
		// Muestra todos los reportes generales sin detalles
		//======================================================
		.get(function (req, res) {
			var	user = req.user,
				init = req.params.init,
				limit = req.params.limit;

			modelreportGlobal.find({})
			.select('id date estacion linea logo')
			.sort({date: -1})
			.skip(init+1)
			.limit(limit)
			.exec(function (err, reports) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}
				res.status(201).send({
					success:true,
					reportes: reports
				})
			})
		})

	router.route('/reporte/general/:id')
		//======================================================
		// Muestra un reporte general en específico a detalle
		//======================================================
		.get(function (req, res) {
			var id = req.params.id;
			modelreportGlobal.findOne({
				_id: id
			})
			.select('-logo')
			.exec(function (err, reportGeneral) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}

				res.status(201).send({
					success:true,
					reporte: reportGeneral
				})
			})
		})

	//======================================================
	// Cuenta reportes emitidos sobre una misma ubicación
	// en un lapso menor a una hora, si existen más de 5
	// reportes bajo las condiciones descritas; se genera
	// un reporte general que acumula los reportes de los
	// usuarios emitidos en el lapso señalado.
	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	// TODO optimizar comparación con menos parámetros
	//======================================================
	function analitic (data) {
		var generalComents = [];
		var generalUsers = [];
		var flag = false;
		// Verificar si ya existe reporte general de esta ubicación en la última hora
		modelreportGlobal.findOne({
			linea: data.linea,
			estacion: data.estacion
		})
		.sort({date: -1})
		.limit(5)
		.exec(function (err, reportGeneral){

			if (err) {
				return console.log(err)
			}

			if (reportGeneral && moment(new Date()).diff(moment(new Date(reportGeneral.date)), 'minutes') < 60) {
				reportGeneral.comentarios.push({
					user_id: data.user_id,
					user_name: data.user_name,
					user_avatar: data.user_avatar,
					date: data.date,
					comentario: data.comentario,
					imagen: data.imagen
				})
				reportGeneral.usuarios.push({
					user_id: data.user_id,
					user_name: data.user_name,
					date: data.date
				})

				reportGeneral.save(function (err){

					if (err) {
						return console.log(err);
					}

					return
				})

			}else{
				// Buscar reportes relacionados con el último enviado
				Report.find({
					linea: data.linea,
					estacion: data.estacion
				})
				.sort({date: -1})
				.limit(10)
				.exec(function (err, reports) {

					if (err) {
						return console.log(err);
					}

					if (reports.length > 0) {
						var logo = reports[0].logo;
						// Evaluar reportes
						for (var i = 0; i < reports.length; i++) {
							var diff = moment(new Date()).diff(moment(new Date(reports[i].date)), 'minutes');
							if(diff < 60){

								generalComents.push({
									user_id: reports[i].user_id,
									user_name: reports[i].user_name,
									user_avatar: reports[i].user_avatar,
									date: reports[i].date,
									comentario: reports[i].comentario,
									imagen: reports[i].imagen
								})

								generalUsers.push({
									user_id: reports[i].user_id,
									user_name: reports[i].user_name,
									date: reports[i].date
								})
							}
						}
						// Si los reportes acumulados son más de 1, crear reporte general
						// Obtener logo de la estacion
						if (generalUsers.length > 1) {
							var params = {
								logo: logo,
								linea: data.linea,
								estacion: data.estacion,
								date: new Date(),
								comentarios: generalComents,
								usuarios: generalUsers
							}

							newReportGeneral = new modelreportGlobal(params);
							newReportGeneral.save(function (err, reportGeneral) {

								if (err) {
									return console.log(err);
								}


							})
						}

					}
				})
			}

		})
	}
}

module.exports = reportController