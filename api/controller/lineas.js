/*lineas.js*/
/*controllers*/

/*
* AmetroMX
* @autor Jesús Hernández 
* Controlador de las líneas
*/

var modelLineas = require('../utils/lineas-metro');

var lineasController = function (app, router) {
	router.route('/lineas')
		//======================================================
		// Registra Lineas nuevas
		//======================================================
		/*.post(function (req, res) {
			var body = req.body,
			    data = {
					linea: body.linea,
					orden: body.orden,
					ruta: body.ruta,
					estaciones:body.estaciones
				};
			modelLineas.findOne({
				linea: body.linea
			})
			.exec(function (err, line){
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error. --/ create linea'
					})
				}

				if (line) {					
					
					line.estaciones = body.estaciones
					
					modelLineas.findOneAndUpdate(
						{
							linea: line.linea
						},{
							estaciones: line.estaciones,
							orden: body.orden
						}
					)
					.exec(function (err, LineUpdated){
						if(err){
							console.log(err)
							return res.status(500).send({
								success: false,
								message: 'Internal server error.'
							})
						}
						return res.status(201).send({
							success:true,
							update:true,
							linea: line
						})
					})

				}else{
					newLinea = new modelLineas(data);
					newLinea.estaciones = [];
					newLinea.save(function (err, Linea) {
						if (err) {
							console.log(err)
							return res.status(500).send({
								success: false,
								message: 'Internal server error.'
							})
						}
						res.status(201).send({
							success:true,
							newLine:true,
							linea: Linea
						})
					})
				}


			})
		})*/
		//======================================================
		// Muestra todas las líneas existentes
		//======================================================
		.get(function (req, res) {
			
			res.status(200).send({
				success:true,
				lineas: modelLineas
			})

		})

	router.route('/linea/:linea')
		//======================================================
		// Muestra información de una línea en específico
		//======================================================
		.get(function (req, res) {
			var linea = req.params.linea,
				flag = false;
			
			for (var i = 0; i < modelLineas.length; i++) {
				if (linea == modelLineas[i].linea) {
					var line = modelLineas[i];
					flag = true;
				}
			}

			if (!flag) {
				return res.status(400).send({
					success: false,
					message: 'Línea '+ linea +' no existe'
				})
			}

			res.status(200).send({
				success: true,
				linea: line
			})
		})

		router.route('/linea/:linea/:estacion')
		//======================================================
		// Muestra información de una línea en específico
		//======================================================
		.get(function (req, res) {
			var linea = req.params.linea,
				estacion = req.params.estacion,
				flag = false;
			
			for (var i = 0; i < modelLineas.length; i++) {
				if (linea == modelLineas[i].linea) {
					var line = modelLineas[i];
					flag = true;
				}	
			}

			if (!flag) {
				return res.status(400).send({
					success: false,
					message: 'Línea '+ linea +' no existe'
				})
			};

			if (!line.estaciones[estacion]) {
				return res.status(400).send({
					success: false,
					message: 'Estación '+ estacion +' no existe'
				})
			}

			res.status(200).send({
				success: true,
				estacion: line.estaciones[estacion]
			})


		})

		//======================================================
		// Elimina una línea en específico
		//======================================================
		/*.delete(function (req, res) {
			var linea = req.params.linea;
			modelLineas.remove({
				linea:linea
			})
			.exec(function (err) {
				if (err) {
					console.log(err)
					return res.status(500).send({
						success: false,
						message: 'Internal server error.'
					})
				}

				res.status(201).send({
					success:true,
					linea: "borrado"
				})
			})
		})*/
}


module.exports = lineasController