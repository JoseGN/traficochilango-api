var gcm = require('node-gcm');
module.exports = gcm;

function gcm(e, cb){

	var message = new gcm.Message();

	message.addData('key1', 'msg1');

	var regIds = ['YOUR_REG_ID_HERE'];

	// Set up the sender with you API key
	var sender = new gcm.Sender('YOUR_API_KEY_HERE');

	//Now the sender can be used to send messages
	sender.send(message, regIds, function (err, result) {
	    if(err){
	    	console.error(err);
	    	return cb(err)	
	    } 

	    console.log(result);
	    cb(null, result)
	});

	sender.sendNoRetry(message, regIds, function (err, result) {
	    if(err){
	    	console.error(err);
	    	return cb(err)	
	    } 

	    console.log(result);
	    cb(null, result)
	});

}
