/* lineas.js */
/* Models */
var config = require('../../config.js');
var mongoose = require('mongoose')

var Schema = mongoose.Schema({
	linea: String,
	orden: String,
	ruta: String,
	estaciones: Object		
	
});

module.exports = mongoose.model('lineas_model', Schema);