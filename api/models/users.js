var config = require('../../config.js');
var mongoose = require('mongoose'),
	bcrypt   = require('bcrypt-nodejs');

var Schema = mongoose.Schema({
	nombre: String,
	email: String,
	password: String,
	followers: Array,
	follows: Array,
	avatar: String,
	estado: String,
	telefono: String,
	redes_sociales: Array,		
	// fb: {
	// 	id: String,
	// 	access_token: String,
	// 	firstName: String,
	// 	lastName: String,
	// 	email: String
	// },
	id_social: String,
	provider: String,
	since: String,
	first_log: String,
	rate_user: Number ,
	rate_global: Number 

});

// == Methods ==
Schema.methods.generateHash = function (password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

Schema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
}

module.exports = mongoose.model('users_model', Schema);