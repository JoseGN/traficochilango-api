/* reporst.js */
/* Models */
var config = require('../../config.js');
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
	linea: String,
	estacion: String,
	logo: String,
	comentario: String,
	imagen: String,
	user: String,
	user_name: String,
	user_avatar: String,
	user_id: String,
	date : String		
	
});

module.exports = mongoose.model('report_model', Schema);