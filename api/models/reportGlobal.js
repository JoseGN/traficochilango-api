/* reportGlobal.js */
/* Models */
var config = require('../../config.js');
var mongoose = require('mongoose');

var Schema = mongoose.Schema({
	logo: String,			
	linea: String,
	estacion: String,	
	date : String, 
	comentarios:[],
	usuarios:[],
})

module.exports = mongoose.model('modelreportGlobal', Schema);