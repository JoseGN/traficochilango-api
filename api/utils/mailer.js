/*
* AmetroMX
* @autor Jesús Hernández 
* Mailer envia correos
*/

// == Dependences ==
var nodemailer = require('nodemailer'),
	transport = require('nodemailer-direct-transport'),
	smtpTransport = require('nodemailer-smtp-transport'),
	transporter = nodemailer.createTransport(smtpTransport()),
	ejs = require('ejs'),
	fs = require('fs');

	var Mailer = function (to, subject, template, content){
		this.to = to;
		this.subject = subject;
		this.template = template;
		this.content = content;
	};

	// ejs.open = '{{';
	// ejs.close = '}}';
	
	var transporter = nodemailer.createTransport(smtpTransport({
	    service: 'gmail',
	    auth: {
	        user: 'ametroMXM@gmail.com',
	        pass: 'pp13034773'
	    }
	}));

	Mailer.prototype.sendMail = function (callback){
		// Obtenemos el template
		var template = process.cwd() + '/api/templates/' + this.template +'.ejs';
		var content = this.content;
		var to = this.to;
		var subject = this.subject;

		// Renderizamos el template

		fs.readFile(template, 'utf8', function (err, file){
			if(err){
				return callback (err);
			} 

			var html = ejs.render(file, content);

			var mailOptions = {
				from: 'no-reply@ametromx.com',
				to: to,
				subject: subject,
				html: html
			};
			transporter.sendMail(mailOptions, function (err, info){
				// Si algo sale mal retornar el error
				if(err) {
					console.log(err);
					return callback(err);
				}

				callback(null, info);
			});
		});
	};

module.exports = Mailer;