// app/utils/tools.js

// == Module dependencies ==

module.exports = {
	date: {
		/**
		*
		* tools.date.getUTCDate
		* Obtiene la hora local
		* @param {Date} date. Objeto date
		* Use: var horaLocal = tools.date.getUTCDate(new Date());
		*/
		getUTCDate: function (date) {
			var newDate = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
			return newDate;
		}
	}, // end tools.date
	math: {
		/**
		*
		* tools.math.getRandomRange
		* Genera un entero aleatorio en un rango [min, max)
		* @return {Integer} Entero aleatorio en un rango [min, max)
		* Use: var iNumeroAleatorio = tools.math.getRandomRange()
		*/
		getRandomRange: function (min, max) {
			return Math.floor(Math.random() * (max - min) + min);
		}
	}, // end tools.math
	password: {
		/**
		*
		* tools.password.generate
		* Genera un password aleatorio de longitud entre 8 y 15
		* Require tools.math.getRandomRange
		* @return {String} Password aleatorio.
		* Use: var sPassword = tools.password.generate()
		*/
		generate: function () {
			var dictionary = "#./0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var password = "";
			
			for (var i = 0; i < getRandomRange(8, 16); i++) {
				password+= dictionary.charAt(getRandomRange(0, dictionary.length +1));
			}

			return password;
		},

		/**
		*
		* tools.password.generateTokenSecret
		* Genera un token aleatorio de longitud entre 15 y 20 caracteres
		* Requiere tools.math.getRandomRange
		* @return {String} Token aleatorio
		* Use: var sTokenSecret = tools.password.generateTokenSecret()
		*/
		generateTokenSecret: function () {
			var dictionary = "._0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var tokenSecret = "";

			for (var i = 0; i < getRandomRange(15, 21); i++) {
				tokenSecret+= dictionary.charAt(getRandomRange(0, dictionary.length + 1));
			}

			return tokenSecret;
		},

		/**
		*
		* tools.password.generateTokenSecretLong
		* Genera un token aleatorio de longitud determinada
		* Requiere tools.math.getRandomRange
		* @param {Integer} logitud desada.
		* @return {String} Token aleatorio
		* Use: var sTokenSecret = tools.password.generateTokenSecret()
		*/
		generateTokenSecretLong: function (longitud) {
			var dictionary = "._0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			var tokenSecret = "";

			for (var i = 0; i < longitud; i++) {
				tokenSecret+= dictionary.charAt(getRandomRange(0, dictionary.length + 1));					
			}
			return tokenSecret
		}
	} // end tools.password
};

function getRandomRange (min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}