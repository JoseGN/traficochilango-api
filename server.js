/*server.js*/

var express = require('express'),
	app = express(),
	mongoose = require('mongoose'),
	flash = require('connect-flash'),
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),
	passport = require('passport'),
	bodyParser = require('body-parser'),
	path = require('path'),
	multer = require('multer'),
	config = require('./config.js'),
	nodemailer = require('nodemailer'),
	transport = require('nodemailer-direct-transport'),
	smtpTransport = require('nodemailer-smtp-transport'),
	transporter = nodemailer.createTransport(transport),
	cors = require('cors')(),
	FacebookStrategy = require('passport-facebook').Strategy;

config.sessionOpts.store = new MongoStore( { mongooseConnection: mongoose.connection });
mongoose.connect(config.db.database);

app.use(bodyParser.json({ extended: true }))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(multer({ inMemory: true })) // Receive files

app.use(session(config.sessionOpts));

// == Required for passport ==
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

var WORKERS = process.env.WEB_CONCURRENCY || 1;
var settings = {};
settings.date = new Date();
app.set('port', (process.env.PORT || config.port));

// == Cors ==
app.use(cors)

// == View Engine ==
// This is for Email templates
app.set('view engine', 'ejs');

// == Router ==
var router = express.Router();

// Controller lines
var lineasController = require('./api/controller/lineas');
lineasController(app, router);
// Controller reports
var reportController = require('./api/controller/reports');
reportController(app, router);
// Controller admin
var adminController = require('./api/controller/admin');
adminController(app, router);
// Controller users
var userController = require('./api/controller/users');
userController(app, router);
// API´s router
app.use('/api/', router);
// Login Social
//var loginFb = require('./api/controller/login_social');
//loginFb(app, router);
// Route API´s Social
//app.use('/auth/', router);

var loginSelf = require('./api/controller/selfauth');
loginSelf(app, router);
// Route API´s auth self
app.use('/auth/self/', router);


// Server ready to listen
app.listen(app.get('port'));
console.log(settings.date.toDateString() + ': Server runing on port : ' + app.get('port'));